variable "vpc_cidr" {
    default = "10.16.0.0/16"
}

variable "subnet_reserved-a_cidr" {
    default = "10.16.0.0/20"
}

variable "subnet_reserved-b_cidr" {
    default = "10.16.16.0/20"
}

variable "subnet_reserved-c_cidr" {
    default = "10.16.32.0/20"
}

variable "subnet_db-a_cidr" {
    default = "10.16.48.0/20"
}

variable "subnet_db-b_cidr" {
    default = "10.16.64.0/20"
}

variable "subnet_db-c_cidr" {
    default = "10.16.80.0/20"
}

variable "subnet_app-a_cidr" {
    default = "10.16.96.0/20"
}

variable "subnet_app-b_cidr" {
    default = "10.16.112.0/20"
}
variable "subnet_app-c_cidr" {
    default = "10.16.128.0/20"
}

variable "subnet_web-a_cidr" {
    default = "10.16.144.0/20"
}

variable "subnet_web-b_cidr" {
    default = "10.16.160.0/20"
}

variable "subnet_web-c_cidr" {
    default = "10.16.176.0/20"
}

variable "type" {
    default = "t2.micro"
}


variable "amiid" {
    default = "ami-09e67e426f25ce0d7"
}

variable "server_port" {
    description = "The port the server will use for HTTP requests"
    type = number
    default = 8080
}

variable "elb_port" {
    description = "The port the ELB will use for HTTP request"
    type = number
    default = 80
    }